# Virtualization Sources

* [Storage\-Performance\-Tuning\-for\-FAST\-Virtual\-Machines\_Fam\-Zheng\.pdf](https://events19.lfasiallc.com/wp-content/uploads/2017/11/Storage-Performance-Tuning-for-FAST-Virtual-Machines_Fam-Zheng.pdf "Storage-Performance-Tuning-for-FAST-Virtual-Machines_Fam-Zheng.pdf")
* [How to Tune QEMU L2 Cache Size and QCOW2 Cluster Size \- IBM Blog](https://www.ibm.com/blog/how-to-tune-qemu-l2-cache-size-and-qcow2-cluster-size/ "How to Tune QEMU L2 Cache Size and QCOW2 Cluster Size - IBM Blog")
