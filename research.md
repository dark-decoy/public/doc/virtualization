My plan is to containerize it. I think if you don't want to do that all you have to do it tell it cert only.
obtain, install, and renew certificates:
    (default) run   Obtain & install a certificate in your current webserver
    certonly        Obtain or renew a certificate, but do not install it
    renew           Renew all previously obtained certificates that are near
third line. certonly mode.

qemu-img create -f qcow2 -o cluster_size=2M -o preallocation=full temp-rfms.qcow2 100G
qemu-img create -f qcow2 -o cluster_size=2M -o preallocation=full template-rfms-2.qcow2 12G

[Back](./readme.md)